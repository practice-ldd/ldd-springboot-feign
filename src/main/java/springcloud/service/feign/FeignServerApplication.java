package springcloud.service.feign;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.ComponentScan;

    @SpringBootApplication
    @EnableEurekaClient
    @EnableFeignClients
    @EnableHystrixDashboard
    @EnableHystrix
    //@ComponentScan(basePackages = {"com.demo2do","com.suidifu", "com.zufangbao"})
    public class FeignServerApplication {

        public static void main(String[] args) {

            SpringApplication.run(FeignServerApplication.class, args);
            System.out.println("启动完毕");

        }
    }
