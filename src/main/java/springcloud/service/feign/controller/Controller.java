package springcloud.service.feign.controller;

import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import springcloud.service.feign.service.HelloService;

/**
 * Created by Raye on 2017/5/22.
 */
@RestController
@RequestMapping(value = "feign")
public class Controller {

    @Resource
    private HelloService helloService;
    /**
     * 远程通过feign调用另外一个项目的方法
     * @param name
     * @return
     */
    @RequestMapping("hello")
    public String hello(String name){
        System.out.println("feign hello");
        return helloService.sayHelloFromRibbon(name);
    }

    /**
     * 使用ribbon方式调用的方法
     * @param name
     * @return
     */
    @RequestMapping("ribbonhello")
    public String sayHello(String name){

        System.out.println("feign ribbonhello");
        return "hello "+name+" this is feign spring cloud";
    }
}